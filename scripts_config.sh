export S3_BUCKET_NAME="axcess-cw-to-s3"
export AWS_REGION="us-east-1"
export REPO_BASE="${BITBUCKET_CLONE_DIR}"
export PROJECT_BASE="${REPO_BASE}/project-base"
export SCRIPTS_BASE="${REPO_BASE}/scripts"
export SERVICE_DIR_LIST="codedeploy monitoring cloudwatch elasticsearch rds"
export OUTPUT_FILE_DIR="outputfile"
export STACK_NAME_PREFIX="CF-INFRA"

