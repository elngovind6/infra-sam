#! /bin/sh

# Description: This Script will act as a master while calling the other dependent scripts.

#The Dependent Script would be Calling multiple scripts located in /scripts directory of the repository


# Checking for the directory named outputfile, that directory will contain all the out tempalted related to all the individual SAM template yaml

#The list of Environment Variables
#REPO_NAME="infra-sam"
#PROJECT_BASE="${REPO_BASE}/project-base"
#SCRIPTS_BASE="${REPO_BASE}/scripts"
#SERVICE_DIR_LIST="codedeploy monitoring cloudwatch elasticsearch rds"
#OUTPUT_FILE_DIR="outputfile"
#STACK_NAME_PREFIX="CF-INFRA"

REPO_BASE="${BITBUCKET_CLONE_DIR}/${REPO_NAME}"
source ${REPO_BASE}/scripts_config.sh

#export S3_BUCKET_NAME="${S3_BUCKET_NAME}"

cd ${PROJECT_BASE} || exit 1

for top_dir in ${SERVICE_DIR_LIST}; do

	if [ -d "$top_dir" ]; then
		
		cd $top_dir
		
		if [ -d "${OUTPUT_FILE_DIR}" ]; then

			echo " "
			echo "Currently in $top_dir directory"

			echo "-------------------->"

			echo "The outputfile already exists"

			echo -e "Removing the old outputfiles\n"

			rm -rvf ${OUTPUT_FILE_DIR}

			echo "Creating a new output fil directory"
			
			mkdir ${OUTPUT_FILE_DIR}


		else

			echo "No Output file exists in $top_dir directory"

			echo "***Creating a new output file***"

			mkdir ${OUTPUT_FILE_DIR}

			echo " ${OUTPUT_FILE_DIR} created in $top_dir directory"

		fi

		cd ..
	fi
done

# Moving all the script files from the script directory to the outputfile directory

#for top_dir in $(find . -type d -maxdepth 1 ! -name '.*' -exec basename {} \;); do


for top_dir  in ${SERVICE_DIR_LIST}; do

	cd $top_dir

#		SCRIPTS_OLD_WD=${pwd}
#
#		cd ${SCRIPTS_BASE}

		echo "***Script Copy Started***"

		echo "***Copying to the scripts to  $top_dir directory ***"

		#cp -vf * ${SCRIPTS_OLD_WD}/$top_dir/${OUTPUT_FILE_DIR}
		
                cp -vf ../../scripts/* ../$top_dir/${OUTPUT_FILE_DIR}

		echo "*** Script Copy Finished in $top_dir directory***"

		echo " Moving to next process -------------->"

#		cd ${SCRIPTS_OLD_WD}
                cd ..
done



# Iterating over all the sub directories of the top level directory and finding all the files that haa .yaml extension to pass that in the script output creation script

for top_dir in ${SERVICE_DIR_LIST}; do

  if [ -d "$top_dir" ]; then

    cd $top_dir

    for sub_dir in *; do

      if [ -d "$sub_dir" ]; then

        cd $sub_dir

	echo "***Switching to $sub_dir of $top_dir***"

	echo "***Searching for all the input yaml files***"

        find . -type f -name "*.yaml" -exec basename {} \; > temp.txt;

	echo "***   Found these yaml files   ***"
	
	cat temp.txt

	echo "***Packaging all the input templates one by one***"
	echo "***Please wait... It may take some time***"

        sh ../../../scripts/script_package.sh "$(cat temp.txt)"

        find . -type f -name "temp.txt" -exec rm -f {} \;

	echo "***   Searching for the parameter files   ***"

	find ../ -type f -name '*.parameter' -exec cat {} \; >> temp_p;

	cat temp_p

#	tr '\n ' ' ' < temp_p >> temp_p1
	tr '\n ' ' ' < temp_p >> output_parameters

	echo "Trimming O/P"

	#sed -e 's/^/"/' -e 's/$/"/' temp_p1 >> output_parameters

	echo "***Found these parameters***"

	cat output_parameters

	mv output_parameters ../${OUTPUT_FILE_DIR}/

	rm -rvf temp*

        cd ..

      fi

    done

    cd ..

  fi

done

#Consolidation of all the individual files into a single file named "consolidated_output""

for out_dir in ${SERVICE_DIR_LIST}; do

	echo "***Consolidation of All the Output templates into one template***"

	echo " "

  if [ -d "$out_dir" ]; then

    echo $out_dir

    cd $out_dir

    echo "***Searching for the Output files***"

    cd ${OUTPUT_FILE_DIR}  

    find . -type f -name "output_*.yaml" -exec basename {} \; > temp_1.txt;

    echo "***Found these outputfiles to be consolidated***"

    cat temp_1.txt

    echo "***Consolidation script getting called***"

    sh script_consolidation_master.sh "$(cat temp_1.txt)"

    find . -type f -name "temp_1.txt" -exec rm -f {} \;

    cd ../..
fi

done


# Deployment of the Final SAM Template


# This template will deploy the cloudformation stack


echo " Please wait...."

echo "******Deploying the stack to Production******"

for top_dir in ${SERVICE_DIR_LIST}; do

	cd $top_dir

	cd ${OUTPUT_FILE_DIR}

	echo "******Passing the required parameters for the stack******"

	sh consolidated_stack_deployment.sh  consolidated_output.yaml ${STACK_NAME_PREFIX}-${top_dir}

	echo "******Deployed to Production******"

	cd ../..

done
