#!/bin/bash

# Example call -
# sh -x script_consolidation_master.sh slack_auto_alert_notification.yaml TagsMonitoring.yaml > consoldiation_run.log 2>&1

# Intermediate Files
temp_sections_list="temp_sections_list.txt"

# Final Output Yaml
output_file="consolidated_output.yaml"

# 1. Getting all different Sections from all different input yaml files
echo "Getting all different Sections"
echo -n "" > $temp_sections_list
cat $temp_sections_list
echo -e "\n"

# file list from input paramters eg.- sh script.sh File1.yaml File2.yaml
file_list="$@"
echo -e "File List: \n$file_list"

# Grep all uniques sections from all yaml input files eg. - Parameters, Resources etc
# these sections don't include the followings -
# AWSTemplateFormatVersion, Transform, Description
for file in $file_list
do
	#echo $file
	#grep "^[[:alnum:]]" $file | grep -E -v "AWSTemplateFormatVersion|Transform|Description" | sed -e s/:// >> $temp_sections_list
	grep "^[[:alnum:]]" $file | grep -E -v "AWSTemplateFormatVersion|Transform|Description"  >> $temp_sections_list
done

echo -e "\nSections List:"

sort -u -o $temp_sections_list $temp_sections_list
cat $temp_sections_list

echo -e "\n\n"

echo "Consolidation of all input Yamls to a single output yaml"
echo -n > $output_file
cat $output_file


# Writing header sections first to the output yaml file

# "AWSTemplateFormatVersion: '2010-09-09'
# Transform: 'AWS::Serverless-2016-10-31'
# Description: An AWS Serverless Specification template describing consolidated functions and resources.

cat <<EOF > $output_file
AWSTemplateFormatVersion: '2010-09-09'
Transform: 'AWS::Serverless-2016-10-31'
Description: An AWS Serverless Specification template describing consolidated functions and resources.
EOF


# Extracting contents from input Yaml files and consolidating in output yaml file
last_section=$(tail -1 $temp_sections_list)

#last_file="${file_list[$(expr ${#file_list[@]}-0)]}"
#last_file=${file_list[@]:(-1)}
#echo "Last file: $last_file and ${#file_list[@]}"
last_file=`for last in $file_list; do true; done; echo -n $last`

for section in `cat $temp_sections_list`
do
	echo "$section" >> $output_file
	for file in $file_list
	do
		echo "$section $file"
		sh -x script_section_extracter.sh $section $file >> $output_file
		if [[ $section == $last_section ]]
		then
			if [[ $file == $last_file ]]
			then
				true
			else
				echo >> $output_file
			fi
		else
			echo >> $output_file
			#true
		fi
	done
done

rm -rf $temp_sections_list
