#!/bin/bash

# exit when any command fails
#set -e

# keep track of the last executed command
#trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
#trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT


S3_BUCKET="${S3_BUCKET_NAME}"
INPUT_FILE_LIST="$@"
OUTPUT_FILE_DIR="outputfile"
echo "$INPUT_FILE_LIST"

for f in $INPUT_FILE_LIST
do

 echo "Processing $f"
 echo "Validating $f"
 aws cloudformation validate-template --template-body file://$f
 aws cloudformation package --template-file $f --s3-bucket ${S3_BUCKET} --s3-prefix ${f%.*} --output-template-file ../${OUTPUT_FILE_DIR}/output_$f
 # do something on $f
done
