#! /bin/sh

#This is a cloudformaion Deploy script, this script will be called by the msaster script

OUTPUT_FILE="$1"
STACK_NAME="$2"
REGION="$AWS_REGION"

aws cloudformation deploy --template-file $OUTPUT_FILE --stack-name $STACK_NAME --region $REGION --parameter-overrides $(cat output_parameters)
