#!/bin/sh

filename="$2"
section="$1"

SECTION_LENGTH=$(echo -n "$section" | tr -d ' ' | wc -m)
section_str=$(echo -n "$section" | cut -c 1-$SECTION_LENGTH)

while IFS='\n' read -r line;

do 
    line_substr=$(echo -n  "$line" | cut -c 1-${SECTION_LENGTH})
    line_substr_ichar=$(echo -n "$line" | cut -c 1)
    
    if [[ "$line_substr" == "$section_str" ]] ; then
        printline="yes"
        # Just t make each line start very clear, remove in use.
        # echo "----------------------->>"
    fi

    if [[ "$line_substr_ichar" != ' ' ]] ; then
       if [[ "$line_substr" != "$section_str" ]] ; then
           printline="no"
           # echo "------------<<"
       fi
    fi
   
    if [[ "$printline" == "yes" ]] ; then
        if [[ "$line_substr_ichar" == ' ' ]] ; then
            echo "$line"
        fi
    fi    

done < "$filename"
    

