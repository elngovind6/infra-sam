import json
import boto3

def lambda_handler(event, context):
    def resources():
        client = boto3.client('rds', region_name='ap-south-1')
        response = client.describe_db_instances(
            )
        id = None
        details = response.get('DBInstances', id)
        return details
    detail = resources() #gets all the rds instances
    list = []
    for instance in detail:
        if instance.get('DBInstanceStatus') == 'available': #if it is running get the arn of the instance
            arn = instance.get('DBInstanceArn') 
            client = boto3.client('rds')
            response = client.list_tags_for_resource(  #get tags of the instance using ARN that we get above
                ResourceName=arn,
            )
            tags = response.get('TagList')
            for tag in tags: 
                if tag.get('Value')=='24hours': # if tag value matches to sandbox then add its dbname to the list
                    list.append(instance.get('DBInstanceIdentifier'))
    
    print(list)  
    details = ""
    for value in list:
        details += value + "\n"
    print (details)
    def sns():
        client = boto3.client('sns', region_name='ap-south-1')
        response = client.publish(
            TopicArn='arn:aws:sns:ap-south-1:294578744445:Rds-sandbox-scheduler-monitoring',
            Message='Hi, \n \n Status of RDS instances in prod account: \n \n'  +str(details) + '\n \n we checked status in the night about above mentioned instances' ,
            Subject='Sandbox RDS instances status',
            )
        return response
    sns()
# return { print (*output, sep = "\n")
#     'statusCode': 200,
#     'body': json.dumps('Please check output!')
# }
    
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
