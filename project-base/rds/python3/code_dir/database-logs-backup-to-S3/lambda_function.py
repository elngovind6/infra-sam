import json
import boto3, botocore, sys
from datetime import datetime
import rdsInstance
import datetime

LASTRECIEVED = 'lastWrittenMarker'
S3BucketName = 'cf-rds-dblogs-backup'
region = 'ap-south-1'

RdsInstances = rdsInstance.RdsInstances

RDSclient = boto3.client('rds',region_name=region)
S3client = boto3.client('s3',region_name=region)



def lambda_handler(event, context):
	indexDt = event['time'].split("T")
	dateFormat = datetime.datetime.strptime(indexDt[0], "%Y-%m-%d")
	indexFormat = '{0}.{1:02}.{2:02}'.format(dateFormat.year, dateFormat.month, dateFormat.day % 100)
	print(indexFormat)
	lastWrittenTime = 0
	lastWrittenThisRun = 0
	try:
		S3response = S3client.head_bucket(Bucket=S3BucketName)
	except botocore.exceptions.ClientError as e:
		error_code = int(e.response['ResponseMetadata']['HTTPStatusCode'])
		if error_code == 404:
			print ("Error: Bucket name provided not found")
			return
		else:
			print ("Error: Unable to access bucket name, error: " + e.response['Error']['Message'])
			return
	for rds in RdsInstances:
		RDSInstanceName = rds['Name']
		S3BucketPrefix = rds['Name'] + '/'
		lastRecievedFile = S3BucketPrefix + LASTRECIEVED
		logtype = rds['Logname']
		try:
			S3response = S3client.get_object(Bucket=S3BucketName, Key=lastRecievedFile)
			lastWrittenTime = int(S3response['Body'].read(S3response['ContentLength']))
			print("Found marker from last log download, retrieving log files with lastWritten time after %s" % str(lastWrittenTime))
		except botocore.exceptions.ClientError as e:
			error_code = int(e.response['ResponseMetadata']['HTTPStatusCode'])
			if error_code == 404:
				print ("It appears this is the first log import, all files will be retrieved from RDS")
			else:
				print ("Error: Unable to access config file, error: " + e.response['Error']['Message'])
				return
			
		copiedFileCount = 0
		logMarker = ""
		moreLogsRemaining = True
		
		while moreLogsRemaining:
			for logNamePrefix in logtype:
				print("logNamePrefix name : " , logNamePrefix)
				dbLogs = RDSclient.describe_db_log_files(DBInstanceIdentifier=RDSInstanceName, FilenameContains=logNamePrefix, FileLastWritten=lastWrittenTime, Marker=logMarker)
				print(dbLogs)
				if 'Marker' in dbLogs and dbLogs['Marker'] != "":
					logMarker = dbLogs['Marker']
				else:
					moreLogsRemaining = False
			
				print("Lenght of logs : ", len(dbLogs['DescribeDBLogFiles']))
				for dbLog in dbLogs['DescribeDBLogFiles']:
					print ("FileNumber: ", copiedFileCount + 1)
					print("Downloading log file: %s found and with LastWritten value of: %s " % (dbLog['LogFileName'],dbLog['LastWritten']))
					if int(dbLog['LastWritten']) > lastWrittenThisRun:
						lastWrittenThisRun = int(dbLog['LastWritten'])
			
			# download the log file
					logFileData = ""
					try:
						logFile = RDSclient.download_db_log_file_portion(DBInstanceIdentifier=RDSInstanceName, LogFileName=dbLog['LogFileName'],Marker='0')
						logFileData = logFile['LogFileData']
						while logFile['AdditionalDataPending']:
							logFile = RDSclient.download_db_log_file_portion(DBInstanceIdentifier=RDSInstanceName, LogFileName=dbLog['LogFileName'],Marker=logFile['Marker'])
							logFileData += logFile['LogFileData']
					except Exception as e:
						print ("File download failed: ", e)
						continue

					logFileDataCleaned = logFileData.encode(errors='ignore')
					logFileAsBytes = str(logFileDataCleaned).encode()

			# upload the log file to S3
					if rds['Engine'] == 'MySQL':
						objectName = S3BucketPrefix + dbLog['LogFileName']  + indexFormat
					else:
						objectName = S3BucketPrefix + dbLog['LogFileName'] 
					try:
						S3response = S3client.put_object(Bucket=S3BucketName, Key=objectName,Body=logFileAsBytes)
						copiedFileCount += 1
					except botocore.exceptions.ClientError as e:
						print ("Error writting object to S3 bucket, S3 ClientError: " + e.response['Error']['Message'])
						return
					print("Uploaded log file %s to S3 bucket %s" % (objectName,S3BucketName))
	
			print ("Copied ", copiedFileCount, "file(s) to s3")

		if lastWrittenThisRun > 0:
			try:
				S3response = S3client.put_object(Bucket=S3BucketName, Key=lastRecievedFile, Body=str.encode(str(lastWrittenThisRun)))			
			except botocore.exceptions.ClientError as e:
				print ("Error writting the config to S3 bucket, S3 ClientError: " + e.response['Error']['Message'])
				return
		print("Wrote new Last Written file to %s in Bucket %s" % (lastRecievedFile,S3BucketName))

	return "Log export completed"
	

