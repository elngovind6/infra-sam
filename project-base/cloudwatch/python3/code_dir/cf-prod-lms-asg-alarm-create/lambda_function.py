import boto3
import json
import os
import re

ENVIRONMENT = 'PROD'
SERVICE_NAME = 'lms windows'

def lambda_handler(event, context):
    print("Boto3 Version: " + boto3.__version__)
    print("Received event: " + json.dumps(event))


    # Preparing variables using Environment Settings
    aws_region = os.getenv('AWS_REGION', 'ap-south-1')
    
    alarmName = os.getenv('CF_ENV_ALARM_BASE_NAME', 'cf-prod-lms-asg-alarm')
    
    default_action_arn = 'arn:aws:sns:ap-south-1:294578744445:cf-test-topic'
    action_arns = []
    for action_arn in os.getenv('CF_ENV_ACTION_ARNS', default_action_arn).split(','):
        action_arns.append(action_arn)
        
    Namespace = os.getenv('CF_ENV_METRIC_NAMESPACE', 'CWAgent')
    
    Disk_MetricName = os.getenv('CF_ENV_METRICNAME_DISK', 'LogicalDisk % Free Space')
    Memory_MetricName = os.getenv('CF_ENV_METRICNAME_MEMORY', 'Memory % Committed Bytes In Use')

    DiskAlarm_ComparisonOperator = os.getenv('CF_ENV_DISKALARM_CMP_OPERATOR', 'LessThanOrEqualToThreshold')
    DiskAlarm_EvaluationPeriods = int(os.getenv('CF_ENV_DISKALARM_EVALUATIONPERIODS', '1'))
    DiskAlarm_Period = int(os.getenv('CF_ENV_DISKALARM_PERIOD', '300'))
    DiskAlarm_Statistic = os.getenv('CF_ENV_DISKALARM_STATISTIC', 'Average')
    DiskAlarm_High_Threshold = int(os.getenv('CF_ENV_DISKALARM_HIGH_THRESHOLD', '20'))
    DiskAlarm_Critical_Threshold = int(os.getenv('CF_ENV_DISKALARM_CRITICAL_THRESHOLD', '10'))
    DiskAlarm_ActionsEnabled = True if os.getenv('CF_ENV_DISKALARM_ACTIONSENABLED', 'True')=='True' else False

    MemoryAlarm_ComparisonOperator = os.getenv('CF_ENV_MEMORYALARM_CMP_OPERATOR', 'GreaterThanOrEqualToThreshold')
    MemoryAlarm_EvaluationPeriods = int(os.getenv('CF_ENV_MEMORYALARM_EVALUATIONPERIODS', '1'))
    MemoryAlarm_Period = int(os.getenv('CF_ENV_MEMORYALARM_PERIOD', '300'))
    MemoryAlarm_Statistic = os.getenv('CF_ENV_MEMORYALARM_STATISTIC', 'Average')
    MemoryAlarm_High_Threshold = int(os.getenv('CF_ENV_MEMORYALARM_HIGH_THRESHOLD', '70'))
    MemoryAlarm_Critical_Threshold = int(os.getenv('CF_ENV_MEMORYALARM_CRITICAL_THRESHOLD', '90'))
    MemoryAlarm_ActionsEnabled = True if os.getenv('CF_ENV_MEMORYALARM_ACTIONSENABLED', 'True')=='True' else False
    
    Memory_Metric_Dimensions_Count = 3 # There was another memory metric with more dimensions than ASG,InstanceId and Objectname

    Alarm_threshold_dict = {
        'HIGH':{
            'Disk':DiskAlarm_High_Threshold,
            'Memory':MemoryAlarm_High_Threshold
        },
        'CRITICAL':{
            'Disk':DiskAlarm_Critical_Threshold,
            'Memory':MemoryAlarm_Critical_Threshold
        }
    }

    # Processing
    CD_client = boto3.client("codedeploy", region_name=aws_region)
    
    applicationName =event["detail"]["application"]
    deploymentGroupName = event["detail"]["deploymentGroup"]
    deploymentId = event["detail"]["deploymentId"]
    
    print('applicationName: '+applicationName)
    print('deploymentGroupName: '+deploymentGroupName)
    print('deploymentId: '+deploymentId)
    
    deployment_group_info = CD_client.get_deployment_group(
            applicationName = applicationName,
            deploymentGroupName = deploymentGroupName
        )
    print(deployment_group_info["deploymentGroupInfo"])
    dg_asg_name = deployment_group_info["deploymentGroupInfo"]["autoScalingGroups"][0].get("name")
    
    print('DG asg_name: '+dg_asg_name)
    
    asg_name_by_deploymentId = 'CodeDeploy_'+deploymentGroupName+'_'+deploymentId
    
    asg_name = asg_name_by_deploymentId
    
    print('asg_name: '+asg_name)

    ASG_client = boto3.client("autoscaling", region_name=aws_region)

    try:
        asg = ASG_client.describe_auto_scaling_groups(
            AutoScalingGroupNames=[
                asg_name
            ]
        )
        if len(asg["AutoScalingGroups"]) > 0:
            asg_name = asg_name
        else:
            try:
                asg = ASG_client.describe_auto_scaling_groups(
                    AutoScalingGroupNames=[
                        dg_asg_name
                    ]
                )
                asg_name = dg_asg_name
            except Exception as asg_ex2:
                print("Call 2. Problem during asg details extraction: "+str(asg_ex2))
    except Exception as asg_ex1:
        print("Call 1. Problem during asg details extraction: "+str(asg_ex1))
        
    print('Final asg_name: '+asg_name)

    instances = asg["AutoScalingGroups"][0]["Instances"]
    instanceIds = [instance["InstanceId"] for instance in instances]
    
    CW_client = boto3.client("cloudwatch", region_name=aws_region)
    
    for index,instanceId in enumerate(instanceIds):

        disk_metric_dict = CW_client.list_metrics(
            Namespace=Namespace,
            MetricName=Disk_MetricName,
            Dimensions=[
                {
                  'Name': 'AutoScalingGroupName',
                  'Value': asg_name
                },
                {
                  'Name': 'InstanceId',
                  'Value': instanceId
                },
            ]
        )
        print('Disk Metric: '+str(disk_metric_dict))

        Cdisk_Metric_Dimensions = {}
        Ddisk_Metric_Dimensions = {}
        
        for metric in disk_metric_dict['Metrics']:
            for dimension in metric['Dimensions']:
                if dimension['Name'] == 'instance':
                    if dimension['Value'] == 'C:':
                        Cdisk_Metric_Dimensions = metric['Dimensions']
                    if dimension['Value'] == 'D:':
                        Ddisk_Metric_Dimensions = metric['Dimensions']


        if Cdisk_Metric_Dimensions:
            
            ALARM_TYPE = 'HIGH'
            AlarmName_C_DISK_Utilization_High = alarmName.replace('<N>',str(index)).replace('<METRIC>','CDRIVE_DISK_UTILIZATION').replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT)

            CW_client.put_metric_alarm(
                AlarmName=AlarmName_C_DISK_Utilization_High,
                ComparisonOperator=DiskAlarm_ComparisonOperator,
                EvaluationPeriods=DiskAlarm_EvaluationPeriods,
                MetricName=Disk_MetricName,
                Namespace=Namespace,
                Period=DiskAlarm_Period,
                Statistic=DiskAlarm_Statistic,
                Threshold=Alarm_threshold_dict[ALARM_TYPE]['Disk'],
                ActionsEnabled=DiskAlarm_ActionsEnabled,
                AlarmActions=action_arns,
                AlarmDescription='Alarm when '+ALARM_TYPE.lower()+' C drive disk usage of '+ENVIRONMENT.lower()+' '+SERVICE_NAME+' instance '+instanceId+' in the ASG '+asg_name+' '+DiskAlarm_ComparisonOperator+' '+str(Alarm_threshold_dict[ALARM_TYPE]['Disk']),
                Dimensions=Cdisk_Metric_Dimensions
            )
            print("Done: Alarm created/updated: "+AlarmName_C_DISK_Utilization_High)
            
            ALARM_TYPE = 'CRITICAL'
            AlarmName_C_DISK_Utilization_Critical = alarmName.replace('<N>',str(index)).replace('<METRIC>','CDRIVE_DISK_UTILIZATION').replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT)

            CW_client.put_metric_alarm(
                AlarmName=AlarmName_C_DISK_Utilization_Critical,
                ComparisonOperator=DiskAlarm_ComparisonOperator,
                EvaluationPeriods=DiskAlarm_EvaluationPeriods,
                MetricName=Disk_MetricName,
                Namespace=Namespace,
                Period=DiskAlarm_Period,
                Statistic=DiskAlarm_Statistic,
                Threshold=Alarm_threshold_dict[ALARM_TYPE]['Disk'],
                ActionsEnabled=DiskAlarm_ActionsEnabled,
                AlarmActions=action_arns,
                AlarmDescription='Alarm when '+ALARM_TYPE.lower()+' C drive disk usage of '+ENVIRONMENT.lower()+' '+SERVICE_NAME+' instance '+instanceId+' in the ASG '+asg_name+' '+DiskAlarm_ComparisonOperator+' '+str(Alarm_threshold_dict[ALARM_TYPE]['Disk']),
                Dimensions=Cdisk_Metric_Dimensions
            )
            print("Done: Alarm created/updated: "+AlarmName_C_DISK_Utilization_Critical)

        if Ddisk_Metric_Dimensions:
            
            ALARM_TYPE = 'HIGH'
            AlarmName_D_DISK_Utilization_High = alarmName.replace('<N>',str(index)).replace('<METRIC>','DDRIVE_DISK_UTILIZATION').replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT)
            
            CW_client.put_metric_alarm(
                AlarmName=AlarmName_D_DISK_Utilization_High,
                ComparisonOperator=DiskAlarm_ComparisonOperator,
                EvaluationPeriods=DiskAlarm_EvaluationPeriods,
                MetricName=Disk_MetricName,
                Namespace=Namespace,
                Period=DiskAlarm_Period,
                Statistic=DiskAlarm_Statistic,
                Threshold=Alarm_threshold_dict[ALARM_TYPE]['Disk'],
                ActionsEnabled=DiskAlarm_ActionsEnabled,
                AlarmActions=action_arns,
                AlarmDescription='Alarm when '+ALARM_TYPE.lower()+' D drive disk usage of '+ENVIRONMENT.lower()+' '+SERVICE_NAME+' instance '+instanceId+' in the ASG '+asg_name+' '+DiskAlarm_ComparisonOperator+' '+str(Alarm_threshold_dict[ALARM_TYPE]['Disk']),
                Dimensions=Ddisk_Metric_Dimensions
            )
            print("Done: Alarm created/updated: "+AlarmName_D_DISK_Utilization_High)
            
            ALARM_TYPE = 'CRITICAL'
            AlarmName_D_DISK_Utilization_Critical = alarmName.replace('<N>',str(index)).replace('<METRIC>','DDRIVE_DISK_UTILIZATION').replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT)
            
            CW_client.put_metric_alarm(
                AlarmName=AlarmName_D_DISK_Utilization_Critical,
                ComparisonOperator=DiskAlarm_ComparisonOperator,
                EvaluationPeriods=DiskAlarm_EvaluationPeriods,
                MetricName=Disk_MetricName,
                Namespace=Namespace,
                Period=DiskAlarm_Period,
                Statistic=DiskAlarm_Statistic,
                Threshold=Alarm_threshold_dict[ALARM_TYPE]['Disk'],
                ActionsEnabled=DiskAlarm_ActionsEnabled,
                AlarmActions=action_arns,
                AlarmDescription='Alarm when '+ALARM_TYPE.lower()+' D drive disk usage of '+ENVIRONMENT.lower()+' '+SERVICE_NAME+' instance '+instanceId+' in the ASG '+asg_name+' '+DiskAlarm_ComparisonOperator+' '+str(Alarm_threshold_dict[ALARM_TYPE]['Disk']),
                Dimensions=Ddisk_Metric_Dimensions
            )
            print("Done: Alarm created/updated: "+AlarmName_D_DISK_Utilization_Critical)
        
        memory_metric_dict = CW_client.list_metrics(
            Namespace=Namespace,
            MetricName=Memory_MetricName,
            Dimensions=[
                {
                  'Name': 'AutoScalingGroupName',
                  'Value': asg_name
                },
                {
                  'Name': 'InstanceId',
                  'Value': instanceId
                },
            ]
        )
        print('Memory Metric: '+str(memory_metric_dict))
        
        Memory_Metric_Dimensions = {}
        for metric in memory_metric_dict['Metrics']:
            Metric_Dimensions = metric['Dimensions']
            if len(Metric_Dimensions) == Memory_Metric_Dimensions_Count:
                Memory_Metric_Dimensions = Metric_Dimensions
        
        if Memory_Metric_Dimensions:
            
            ALARM_TYPE = 'HIGH' 
            AlarmName_MEM_Utilization_High = alarmName.replace('<N>',str(index)).replace('<METRIC>','MEM_UTILIZATION').replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT)
            
            CW_client.put_metric_alarm(
                AlarmName=AlarmName_MEM_Utilization_High,
                ComparisonOperator=MemoryAlarm_ComparisonOperator,
                EvaluationPeriods=MemoryAlarm_EvaluationPeriods,
                MetricName=Memory_MetricName,
                Namespace=Namespace,
                Period=MemoryAlarm_Period,
                Statistic=MemoryAlarm_Statistic,
                Threshold=Alarm_threshold_dict[ALARM_TYPE]['Memory'],
                ActionsEnabled=MemoryAlarm_ActionsEnabled,
                AlarmActions=action_arns,
                AlarmDescription='Alarm when '+ALARM_TYPE.lower()+' memory usage of '+ENVIRONMENT.lower()+' '+SERVICE_NAME+' instance '+instanceId+' in the ASG '+asg_name+' '+MemoryAlarm_ComparisonOperator+' '+str(Alarm_threshold_dict[ALARM_TYPE]['Memory']),
                Dimensions=Memory_Metric_Dimensions
            )
            print("Done: Alarm created/updated: "+AlarmName_MEM_Utilization_High)
            
            ALARM_TYPE = 'CRITICAL' 
            AlarmName_MEM_Utilization_Critical = alarmName.replace('<N>',str(index)).replace('<METRIC>','MEM_UTILIZATION').replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT)
            
            CW_client.put_metric_alarm(
                AlarmName=AlarmName_MEM_Utilization_Critical,
                ComparisonOperator=MemoryAlarm_ComparisonOperator,
                EvaluationPeriods=MemoryAlarm_EvaluationPeriods,
                MetricName=Memory_MetricName,
                Namespace=Namespace,
                Period=MemoryAlarm_Period,
                Statistic=MemoryAlarm_Statistic,
                Threshold=Alarm_threshold_dict[ALARM_TYPE]['Memory'],
                ActionsEnabled=MemoryAlarm_ActionsEnabled,
                AlarmActions=action_arns,
                AlarmDescription='Alarm when '+ALARM_TYPE.lower()+' memory usage of '+ENVIRONMENT.lower()+' '+SERVICE_NAME+' instance '+instanceId+' in the ASG '+asg_name+' '+MemoryAlarm_ComparisonOperator+' '+str(Alarm_threshold_dict[ALARM_TYPE]['Memory']),
                Dimensions=Memory_Metric_Dimensions
            )
            print("Done: Alarm created/updated: "+AlarmName_MEM_Utilization_Critical)
        
        CPUUtilization_Metric_Dimensions=[
            {
                'Name': 'InstanceId',
                'Value': instanceId
            },
        ]

        print('CPU Utilization Metric Dimensions: '+str(CPUUtilization_Metric_Dimensions))
        
        if CPUUtilization_Metric_Dimensions:
            
            ALARM_TYPE = 'HIGH'
            AlarmName_CPUUtilization_High = alarmName.replace('<N>',str(index)).replace('<METRIC>','CPU_UTILIZATION').replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT)
            
            CW_client.put_metric_alarm(
                AlarmName=AlarmName_CPUUtilization_High,
                ComparisonOperator="GreaterThanOrEqualToThreshold",
                EvaluationPeriods=1,
                MetricName="CPUUtilization",
                Namespace="AWS/EC2",
                Period=300,
                Statistic="Average",
                Threshold=60,
                ActionsEnabled=True,
                AlarmActions=action_arns,
                AlarmDescription='Alarm when '+ALARM_TYPE.lower()+' cpu usage of '+ENVIRONMENT.lower()+' '+SERVICE_NAME+' instance '+instanceId+' in the ASG '+asg_name+' '+'GreaterThanOrEqualToThreshold'+' '+' 60',
                Dimensions=CPUUtilization_Metric_Dimensions
            )
            print("Done: Alarm created/updated: "+AlarmName_CPUUtilization_High)
            
            ALARM_TYPE = 'CRITICAL'
            AlarmName_CPUUtilization_Critical = alarmName.replace('<N>',str(index)).replace('<METRIC>','CPU_UTILIZATION').replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT)
            
            CW_client.put_metric_alarm(
                AlarmName=AlarmName_CPUUtilization_Critical,
                ComparisonOperator="GreaterThanOrEqualToThreshold",
                EvaluationPeriods=1,
                MetricName="CPUUtilization",
                Namespace="AWS/EC2",
                Period=300,
                Statistic="Average",
                Threshold=90,
                ActionsEnabled=True,
                AlarmActions=action_arns,
                AlarmDescription='Alarm when '+ALARM_TYPE.lower()+' cpu usage of '+ENVIRONMENT.lower()+' '+SERVICE_NAME+' instance '+instanceId+' in the ASG '+asg_name+' '+'GreaterThanOrEqualToThreshold'+' '+' 90',
                Dimensions=CPUUtilization_Metric_Dimensions
            )
            print("Done: Alarm created/updated: "+AlarmName_CPUUtilization_Critical)
        
        
    print("Done: Alarms created/updated")

    # to count number of particular type of metric alarms (use any of memory/disk/cpu)
    
    print("Begin to delete unused alarms.")
    print("First count unused alarms using any one of the Mem/Disk/CPU metric type alarms.")
    
    single_type_alarms_all_index_set = set()
    SINGLE_METRIC_TYPE_ALARMS_COUNT = 0
    try:
        alarms_desc_response = CW_client.describe_alarms(
            AlarmNamePrefix='LMS-NODE',
            MaxRecords=100
        )
        NextToken = alarms_desc_response.get('NextToken')
        
        #SINGLE_METRIC_TYPE_ALARMS_COUNT = SINGLE_METRIC_TYPE_ALARMS_COUNT + len(alarms_desc_response["MetricAlarms"])
        
        for inx in range(len(alarms_desc_response['MetricAlarms'])):
            rgx = re.match(r'LMS-NODE.-MEM_UTILIZATION-HIGH-'+ENVIRONMENT, alarms_desc_response['MetricAlarms'][inx]['AlarmName'])
            if rgx:
                single_type_alarms_all_index_set.add(rgx.group(0).split('-')[1].replace('NODE', ''))
            #alarms_desc_response['MetricAlarms'][inx]['AlarmConfigurationUpdatedTimestamp'] = alarms_desc_response['MetricAlarms'][inx]['AlarmConfigurationUpdatedTimestamp'].isoformat()
            #alarms_desc_response['MetricAlarms'][inx]['StateUpdatedTimestamp'] = alarms_desc_response['MetricAlarms'][inx]['StateUpdatedTimestamp'].isoformat()
            
        #print("Alarms Desc Response Call 1: "+json.dumps(alarms_desc_response))
        
        try:
            while NextToken:
                alarms_desc_response = CW_client.describe_alarms(
                    AlarmNames=[
                        alarmName.replace('<N>',str(index)).replace('<METRIC>','MEM_UTILIZATION').replace('<ALARM>', 'HIGH').replace('<ENV>', ENVIRONMENT),
                    ],
                    MaxRecords=100
                )
                NextToken = alarms_desc_response.get('NextToken')
                
                #SINGLE_METRIC_TYPE_ALARMS_COUNT = SINGLE_METRIC_TYPE_ALARMS_COUNT + len(alarms_desc_response["MetricAlarms"])
                
                for inx in range(len(alarms_desc_response['MetricAlarms'])):
                    rgx = re.match(r'LMS-NODE.-MEM_UTILIZATION-HIGH-'+ENVIRONMENT, alarms_desc_response['MetricAlarms'][inx]['AlarmName'])
                    if rgx:
                        single_type_alarms_all_index_set.add(rgx.group(0).split('-')[1].replace('NODE', ''))
                    #alarms_desc_response['MetricAlarms'][inx]['AlarmConfigurationUpdatedTimestamp'] = alarms_desc_response['MetricAlarms'][inx]['AlarmConfigurationUpdatedTimestamp'].isoformat()
                    #alarms_desc_response['MetricAlarms'][inx]['StateUpdatedTimestamp'] = alarms_desc_response['MetricAlarms'][inx]['StateUpdatedTimestamp'].isoformat()
                    
                #print("Alarms Desc Response Call Next: "+json.dumps(alarms_desc_response))
                
        except Exception as alarms_desc_ex2:
            print("Call 2. Problem during alarms description extraction: "+str(alarms_desc_ex2))
            
    except Exception as alarms_desc_ex1:
        print("Call 1. Problem during alarms description extraction: "+str(alarms_desc_ex1))
        
    SINGLE_METRIC_TYPE_ALARMS_COUNT = len(single_type_alarms_all_index_set)
    
    print('SINGLE_METRIC_TYPE_ALARMS_COUNT: '+str(SINGLE_METRIC_TYPE_ALARMS_COUNT))
        
    print("Done Unused Alarms Counting")
    
    NUMBER_OF_INSTANCES_IN_ASG = len(instanceIds)
    
    print("Next to delete Unused Alarms.")
    if SINGLE_METRIC_TYPE_ALARMS_COUNT > NUMBER_OF_INSTANCES_IN_ASG:
        UNUSED_ALARMS_COUNT = SINGLE_METRIC_TYPE_ALARMS_COUNT - NUMBER_OF_INSTANCES_IN_ASG
        
        print("List Unused Alarms Names.")
        AlarmNames = []
        for unused_index in range(NUMBER_OF_INSTANCES_IN_ASG, SINGLE_METRIC_TYPE_ALARMS_COUNT):
            for ALARM_TYPE in ['HIGH', 'CRITICAL']:
                for METRIC in ['CDRIVE_DISK_UTILIZATION', 'DDRIVE_DISK_UTILIZATION', 'MEM_UTILIZATION', 'CPU_UTILIZATION']:
                    
                    AlarmNames.append(alarmName.replace('<N>',str(unused_index)).replace('<METRIC>',METRIC).replace('<ALARM>', ALARM_TYPE).replace('<ENV>', ENVIRONMENT))
                    

        print("Deleting Unused Alarms: "+str(AlarmNames))
        try:
            alarms_delete_response = CW_client.delete_alarms(
                AlarmNames=AlarmNames
            )
            
            print("Alarms Delete Response: "+json.dumps(alarms_delete_response))
            
        except Exception as alarms_del_ex:
            print("Problem during alarms deletion: "+str(alarms_del_ex))
    
    print("Done. Deleted Unused Alarms.")

