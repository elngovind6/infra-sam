import json
import boto3
import os
from datetime import datetime, timedelta
import logs

logGroups = logs.logGroups

AWS_REGION = os.getenv('AWS_REGION', 'ap-south-1')

NUMBER_OF_DAYS_TO_STEP_BACK_FOR_BACKUP = int(os.getenv('CF_ENV_NUMBER_OF_DAYS_TO_STEP_BACK_FOR_BACKUP', '1'))

DAYS_DURATION_OF_BACKUP = int(os.getenv('CF_ENV_DAYS_DURATION_OF_BACKUP', '1')) #45 set  Schedule this lambda function according to this duration

logs_client = boto3.client('logs', region_name=AWS_REGION)

epoch = datetime.utcfromtimestamp(0)
now = datetime.utcnow()
action_date = (now - timedelta(days=NUMBER_OF_DAYS_TO_STEP_BACK_FOR_BACKUP)).date()
action_datetime = datetime(action_date.year, action_date.month, action_date.day)
action_day_start_time = int((action_datetime - epoch).total_seconds() * 1000.0)
action_day_end_time = action_day_start_time + 86400000 * (DAYS_DURATION_OF_BACKUP - 1) + 86399999

DATE_PREFIX = action_date.isoformat().replace('-','/')
if DAYS_DURATION_OF_BACKUP > 1:
    DATE_PREFIX = DATE_PREFIX+'+'+str(DAYS_DURATION_OF_BACKUP-1)

def lambda_handler(event, context):

    print("Received Event: "+json.dumps(event))
    
    ACCOUNT_ID = context.invoked_function_arn.split(":")[4]
    
    loggroup_index = 0
    loggroup_name = logGroups[loggroup_index]
    
    if event.get("data"):
        loggroup_index = event.get("data")["loggroup_index"]
        
        TASKID = event.get("data").get("taskId")
        
        if TASKID:
            try:
                describe_export_tasks_response = logs_client.describe_export_tasks(
                    taskId=TASKID
                )
                print("Describe Export Task Response "+json.dumps(describe_export_tasks_response))
            except Exception as ex2:
                print("Describe_export_task failed with exception "+str(ex2))
                
            TASK_STATUS = describe_export_tasks_response["exportTasks"][0]["status"]["code"]
            
            if TASK_STATUS != 'PENDING' and TASK_STATUS != 'RUNNING':
                
                if loggroup_index < (len(logGroups)-1):
                    
                    loggroup_index = loggroup_index + 1
                    loggroup_name = logGroups[loggroup_index]
                    
                    try:
                        creat_export_task_response = logs_client.create_export_task(
                            logGroupName=loggroup_name,
                            #logStreamNamePrefix=logGroupStream,
                            fromTime=action_day_start_time,
                            to=action_day_end_time,
                            destination='cf-cloudwatch-logs-backup',
                            destinationPrefix=ACCOUNT_ID+'/'+loggroup_name.lstrip('/')+'/'+DATE_PREFIX
                        )
                        print("Create Export Task Response "+json.dumps(creat_export_task_response))
                    except Exception as ex3:
                        print("Create_export_task failed with exception "+str(ex3))
                        
                    return {
                        'taskId': creat_export_task_response.get("taskId"),
                        'loggroup_index': loggroup_index,
                        'end': 'no'
                    }
                else:
                    return {
                        'taskId': TASKID,
                        'loggroup_index': loggroup_index,
                        'end': 'yes'
                    }
            else:
                return {
                    'taskId': TASKID,
                    'loggroup_index': loggroup_index,
                    'end': 'no'
                }
        else:
            return {
                'taskId': TASKID,
                'loggroup_index': loggroup_index,
                'end': 'yes'
            }
    else:
        try:
            creat_export_task_response = logs_client.create_export_task(
                logGroupName=loggroup_name,
                #logStreamNamePrefix=logGroupStream,
                fromTime=action_day_start_time,
                to=action_day_end_time,
                destination='cf-cloudwatch-logs-backup',
                destinationPrefix=ACCOUNT_ID+'/'+loggroup_name.lstrip('/')+'/'+DATE_PREFIX
            )
            print("Create Export Task Response "+json.dumps(creat_export_task_response))
        except Exception as ex1:
            print("Create_export_task failed with exception "+str(ex1))
            
        return {
            'taskId': creat_export_task_response.get("taskId"),
            'loggroup_index': loggroup_index,
            'end': 'no'
        }
