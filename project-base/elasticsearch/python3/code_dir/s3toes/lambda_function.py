##################################################
### Elasticsearch host name
ES_HOST = "search-prod-alb-logs-elk-z3ne3n2x2kys3gq2c4zo2pnq2i.ap-south-1.es.amazonaws.com"
 
### Elasticsearch prefix for index name
INDEX_PREFIX = {
    "PROD_PUB": "prod-public-alb",
    "PROD_PRIV": "prod-private-alb",
    "PROD_PUB_SAFE": "prod-public-safe-alb",
    "PROD_PUB_LMS": "prod-public-lms-alb",
    "PROD_PUB_FLASK": "prod-public-flask-alb",
    "PROD_PRIV_SAFE": "prod-private-safe-alb",
    "PROD_PRIV_LMS": "prod-private-lms-alb",
    "PROD_PRIV_FLASK": "prod-private-flask-alb",
    "AMAZON": "amazon-alb",
    "DS_PYTHON": "Ds-python-alb"
}

### ALB name for type name
ALB_NAME_KEYS = {
    "prod-public": "PROD_PUB",
    "prod-private": "PROD_PRIV",
    "prod-public-safe": "PROD_PUB_SAFE",
    "prod-public-lms": "PROD_PUB_LMS",
    "prod-public-flask": "PROD_PUB_FLASK",
    "prod-private-safe": "PROD_PRIV_SAFE",
    "prod-private-lms": "PROD_PRIV_LMS",
    "prod-private-flask": "PROD_PRIV_FLASK",
    "amazon": "AMAZON",
    "Ds-python": "DS_PYTHON"
}
 
### Enabled to change timezone. If you set UTC, this parameter is blank
TIMEZONE = "Asia/Kolkata"
 
#################################################
### ELB access log format keys
ALB_KEYS = ["type", "@timestamp", "elb", "client_ip", "client_port", "target_ip", "target_port", "request_processing_time", "target_processing_time", "response_processing_time", "elb_status_code", "target_status_code", "received_bytes", "sent_bytes", "request_verb", "request_url", "request_proto", "user_agent", "ssl_cipher", "ssl_protocol", "target_group_arn", "trace_id", "domain_name", "chosen_cert_arn", "matched_rule_priority", "request_creation_time", "actions_executed", "redirect_url"]
### ELB access log format regex
ALB_REGEX = r'^([a-z2]+)\s+([\d-]+T[\d:]+[.\d]+Z)\s+([a-zA-Z\/0-9-]+)\s+([\d.]+):([\d]+)\s+([\d.-]+)[:]*([\d-]*)\s+([\d.\d-]+)\s+([\d.\d-]+)\s+([\d.\d-]+)\s+([\d-]+)\s+([\d-]+)\s+([\d]+)\s+([\d]+)\s+\"([A-Z-]+)\s+([!*.:,a-zA-Z\/0-9\?%=\&@$+_-]+)\s+([A-Za-z\/\d.\d-]+)\"\s+\"([+\s:;~,_a-zA-Z\/\d.\(\)-]+)\"\s+([A-Za-z\d-]+)\s+([-A-Za-z.\d]+)\s*([a-z:\d\/-]*)\s*\"*([A-Za-z;\d=-]*)\"*\s*\"*([A-Za-z\d.-]*)\"*\s*\"*([a-z:\d\/-]*)\"*\s*([\d-]*)\s*([\d-]*[T]*[\d:]*[.\d]*[Z]*)\s*\"*([a-zA-Z-]*)\"*\s*\"*([!*.:,a-zA-Z\/0-9\?%=\&@$+_-]*)\"*'
 
#################################################


import json
import boto3
import io
import gzip
import sys
import os
import re
from datetime import datetime
from dateutil import parser, tz, zoneinfo
import requests
import hashlib, hmac

R = re.compile(ALB_REGEX)

def lambda_handler(event, context):
    # TODO implement
    print("Boto3 Version: " + boto3.__version__)
    print("Received event: " + json.dumps(event))

    eventTime = event["Records"][0]["eventTime"]
    eventDate = eventTime.split("T")[0].replace('-', '.')
    
    log_drop_timestamp = parser.parse(eventTime).isoformat()
    
    s3_event = event["Records"][0]["s3"]
    
    bucket_name = s3_event["bucket"]["name"]
    s3_key = s3_event["object"]["key"]
    
    filename = s3_event["bucket"]["name"] + '/' + s3_event["object"]["key"]
    
    print(filename)
    
    ALB_NAME = s3_key.split('/')[0]
    
    INDEX_PREFIX_KEY = ALB_NAME_KEYS[ALB_NAME]

    INDEX = INDEX_PREFIX[INDEX_PREFIX_KEY] + "-" + eventDate
    print (INDEX)
    
    TYPE = ALB_NAME
    
    s3_client = boto3.client('s3', region_name='ap-south-1')
    
    try:
        s3_get_object_response = s3_client.get_object(
                                Bucket=bucket_name,
                                Key=s3_key,
                            )
        #print("S3 get object response "+json.dumps(s3_get_object_response))
        print("Get access log from S3.")
    except Exception as e:
        print("Exception in s3 get object "+str(e))
        
    if s3_get_object_response:
        print(s3_get_object_response["ContentLength"])
        
    
        buffer = io.BytesIO(s3_get_object_response["Body"].read())
        
        try:
            extracted_content = gzip.GzipFile(fileobj=buffer)
        except Exception as e:
            print("Extracting zipfile exception "+str(e))
            
        print("Zip file content")
        #print(extracted_content.read().decode('utf-8'))
        read_content = extracted_content.read().decode('utf-8').split('\n')
        print("Zip content reading done.")
        print(type(read_content))
        
        data = ""
    
        for index, content in enumerate(read_content):
            #if index == 0 or index == 4:
                #print("read_content index "+str(index))
                #print("content "+content)
                
            match = R.match(content)
            if not match:
                #continue
                print('Not Match ------------------')
                print(content)
                if not content or content.isspace():
                    print("Continue ...........")
                    continue
                values = ['-',]*28
                doc = dict(zip(ALB_KEYS, values))
            else:
                try:
                    values = match.groups(0)
                except Exception as e:
                    print("Regex exception -------------------- "+str(e))
                
                doc = dict(zip(ALB_KEYS, values))
                
                #if TIMEZONE:
                    #doc[ALB_KEYS[1]] = parser.parse(doc[ALB_KEYS[1]]).replace(tzinfo=tz.tzutc()).astimezone(zoneinfo.gettz(TIMEZONE)).isoformat()
                doc[ALB_KEYS[1]] = parser.parse(doc[ALB_KEYS[1]]).isoformat()
            
            doc['log_drop_timestamp'] = log_drop_timestamp
            doc['@message'] = content
            
            data += '{"index":{"_index":"' + INDEX + '","_type":"' + TYPE + '"}}\n'
            data += json.dumps(doc) + "\n"

            #print("DOC ----------------")
            #print(doc)
            
            if len(data) > 1000000:
                print("Data type, length check")
                print (type(data))
                _bulk(ES_HOST, data)
                data = ""
                
        if data:
            print("Data type check")
            print (type(data))
            #print (data)
            _bulk(ES_HOST, data)

def _bulk(host, doc):
    response = buildRequest(host, doc)
    print("RESPONSE ------------------------------")
    print (response.status_code)
    #print (response.text)

def _create_url(host, path, ssl=False):
    if not path.startswith("/"):
        path = "/" + path
        
    if ssl:
        return "https://" + host + path
    else:
        return "http://" + host + path

def sign(key, msg):
    return hmac.new(key, msg.encode("utf-8"), hashlib.sha256).digest()
    
def getSignatureKey(key, date_stamp, regionName, serviceName):
    kDate = sign(('AWS4' + key).encode('utf-8'), date_stamp)
    kRegion = sign(kDate, regionName)
    kService = sign(kRegion, serviceName)
    kSigning = sign(kService, 'aws4_request')
    return kSigning

def buildRequest(host, body):
    
    url = _create_url(host, '/_bulk', ssl=True)
    print("url "+url)
    
    access_key = os.environ.get('AWS_ACCESS_KEY_ID')
    secret_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
    session_token = os.environ.get('AWS_SESSION_TOKEN')
    #print("keys: "+access_key+", "+secret_key+", "+session_token)
    
    if access_key is None or secret_key is None:
        print('No access key is available.')
        sys.exit()
    content_type = 'application/json'
    content_length = str(len(body))
    method = 'POST'
    
    endpointParts = re.match(r'^([^\.]+)\.?([^\.]*)\.?([^\.]*)\.amazonaws\.com$', host)
    region = endpointParts.group(2)
    print("Buildreq Region "+region)
    service = endpointParts.group(3)
    print("Buildreq service "+service)
    t = datetime.utcnow()
    amz_date = t.strftime('%Y%m%dT%H%M%SZ')
    print("amz_date "+amz_date)
    date_stamp = t.strftime('%Y%m%d')
    print("date_stamp "+date_stamp)
    signing_key = getSignatureKey(secret_key, date_stamp, region, service)
    #print("signing_key "+str(signing_key))
    
    '''
    request = {
        'host': host,
        'method': 'POST',
        'path': '/_bulk',
        'body': body,
        'headers': { 
            'Content-Type': content_type,
            'Host': host,
            'Content-Length': content_length,
            'X-Amz-Security-Token': session_token,
            'X-Amz-Date': amz_date
        }
    }
    '''
    
    canonical_uri = '/_bulk'
    #print("canonical_uri "+canonical_uri)
    
    canonical_querystring = ''
    #print("canonical_querystring "+canonical_querystring)

    canonical_headers = 'content-length:' + content_length + '\n' + 'content-type:' + content_type + '\n' + 'host:' + host + '\n' + 'x-amz-date:' + amz_date + '\n' + 'x-amz-security-token:' + session_token + '\n'
    #canonical_headers = 'content-type:' + content_type + '\n' + 'host:' + host + '\n' + 'x-amz-date:' + amz_date + '\n' + 'x-amz-security-token:' + session_token + '\n'
    #print("canonical_headers "+canonical_headers)
    
    signed_headers = 'content-length;content-type;host;x-amz-date;x-amz-security-token'
    #signed_headers = 'content-type;host;x-amz-date;x-amz-security-token'
    #print("signed_headers "+signed_headers)
    
    payload_hash = hashlib.sha256(body.encode('utf-8')).hexdigest()
    #print("payload_hash "+payload_hash)
    
    canonical_request = method + '\n' + canonical_uri + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash
    #print("canonical_request "+canonical_request)
    
    algorithm = 'AWS4-HMAC-SHA256'
    credential_scope = date_stamp + '/' + region + '/' + service + '/' + 'aws4_request'
    string_to_sign = algorithm + '\n' +  amz_date + '\n' +  credential_scope + '\n' +  hashlib.sha256(canonical_request.encode('utf-8')).hexdigest()
    #print("string_to_sign "+string_to_sign)
    
    signature = hmac.new(signing_key, (string_to_sign).encode('utf-8'), hashlib.sha256).hexdigest()
    #print("signature "+signature)

    authorization_header = algorithm + ' ' + 'Credential=' + access_key + '/' + credential_scope + ', ' +  'SignedHeaders=' + signed_headers + ', ' + 'Signature=' + signature
    #print("authorization_header "+authorization_header)

    headers = {
           'Content-Type':content_type,
           'Host': host,
           'Content-Length': content_length,
           'X-Amz-Date':amz_date,
           'X-Amz-Security-Token': session_token,
           'Authorization':authorization_header
        }
    #print("headers "+str(headers))

    return requests.post(url, data=body, headers=headers);
