import requests
import os
from datetime import datetime
import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
import logging
import curator
import configuration



logger = logging.getLogger('curator')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)


region = 'ap-south-1'
service = 'es' 
credentials = boto3.Session().get_credentials() 
awsauth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)
now = datetime.now()
date_string = '-'.join((str(now.year), str(now.month), str(now.day), str(now.hour), str(now.minute)))
snapshot_name = 'elasticsearch_snapshot-' + date_string
HOST = os.environ['host']



### creating snapshot of the indices

def snapshotCreation():
	es = Elasticsearch(
		hosts = HOST,
		http_auth = awsauth,
		use_ssl = True,
		verify_certs = True,
		connection_class = RequestsHttpConnection,
		timeout = 2700
	)
	try:
		index_list = curator.IndexList(es)
		snap_indices = curator.Snapshot(index_list, repository="my-snapshot-repo", name=snapshot_name, wait_for_completion=True, max_wait =600, wait_interval=3)
		snap_indices.do_action()
		snap_indices_status = snap_indices.get_state()
		print(snap_indices_status)
	except (curator.exceptions.SnapshotInProgress, curator.exceptions.FailedExecution) as e:
		print("Exception during creation of snapshot: ", str(e))
		
	return  snap_indices_status    



### deletion of indices after specific date

def deleteIndices():
	es = Elasticsearch(
		hosts = HOST,
		http_auth = awsauth,
		use_ssl = True,
		verify_certs = True,
		connection_class = RequestsHttpConnection,
		timeout = 2700
	)
	try:
		index_list = curator.IndexList(es)
		index_list.filter_by_age(source='name', direction='older', timestring='%Y.%m.%d', unit='days', unit_count=90)
		if index_list.indices:
			curator.DeleteIndices(index_list).do_action()
	except(curator.exceptions.FailedExecution) as e:
		print("Exception while deleting indices: ", str(e))
				
				
def lambda_handler(event, context):
	
	if event.get("data"):
		condition = event.get("data")["Condition"]
		if condition == 'snapshot_creation_and_deletion_of indices':
			print("The COndition : ", condition)
			Status = snapshotCreation()
			if Status == "SUCCESS":
				deleteIndices()
				return{
					'Condition' : 'snapshot_creation_and_deletion_of indices',
					'end' : 'yes'
				}
									 
			else:
				return{
					'Condition' : 'snapshot_creation_and_deletion_of indices',
					'end' : 'no'
				}
									 
		if condition == 'snapshot_creation':
			Status = snapshotCreation()
			if Status == "SUCCESS":
				return{
					'Condition' : 'snapshot_creation',
					'end' : 'yes'
				}
			
			else:
				return{
					'Condition' : 'snapshot_creation',
					'end' : 'no'
				}
			
		if condition == 'delete_indices':
			deleteIndices()
			return {
				'Condition': 'delete_indices',
				'end' : 'yes'
			}
				
							 
	else :
		if configuration.config['CreateSnapshot_And_DeleteIndices'] == True:
			return{
				'Condition' : 'snapshot_creation_and_deletion_of indices',
				'end' : 'no'
			}
						
					 
		elif configuration.config['CreateSnapshot_And_No_DeleteIndices'] == True:
			return{
				'Condition' : 'snapshot_creation',
				'end' : 'no'
			}
						
		elif configuration.config['DeleteIndices_And_No_SnapshotCreation'] == True:
			return{
				'Condition': 'delete_indices',
				'end' : 'no'
			}
				
				
