import os
import requests
import datetime
import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
from datetime import timedelta
from dateutil import tz

# credentials for signature
host = os.environ['host']
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(credentials.access_key, credentials.secret_key, 'ap-south-1', 'es', session_token=credentials.token)

# Environment variables
ELASTICSEARCH_URL = os.environ['es_url']
INDEX = os.environ['index']
RESPONSETYPE1 = os.environ['response_type1']
RESPONSETYPE2 = os.environ['response_type2']
PROCESSINGTIME = os.environ['processingTime']
TOPICARN = os.environ['topicArn']
PROCESSINGTIME1 = os.environ['processingTime_1']
PROCESSINGTIME2 = os.environ['processingTime_2']
PROCESSINGTIME3 = os.environ['processingTime_3']
OPERATIONNAME_LIST = [os.environ['registration'], os.environ['standingInstructionSetup'],os.environ['chargeAuthorization'], os.environ['chargeCancelAuthorization'],os.environ['chargeCapture'], os.environ['chargeCancelCapture']]
OPERATIONNAME2 = os.environ['operationName_2']
OPERATIONNAME3 = os.environ['operationName_3']
OPERATIONTYPE1 = os.environ['operationType_1']
OPERATIONTYPE2 = os.environ['operationType_2']
PARTNER_ID = os.environ['partnerId']

# SNS client
client = boto3.client('sns')

# Connect to ES
es = Elasticsearch(hosts=[ELASTICSEARCH_URL], http_auth=awsauth, use_ssl=True, verify_certs=True,connection_class=RequestsHttpConnection)

messageRes = ''
subjectRes = ''
subjectInt = ''
messageInt = ''
subjectProc = ''
messageProc = ''
ResCount = ''
IntCount = ''
ProCount = ''


def lambda_handler(event, context):
    print(event)
    global messageRes, subjectRes, subjectInt, messageInt, subjectProc, messageProc, ResCount, IntCount, ProCount
    # Index for ES Search
    indexDt = event['time'].split("T")
    dateFormat = datetime.datetime.strptime(indexDt[0], "%Y-%m-%d")
    indexFormat = '{0}.{1:02}.{2:02}'.format(dateFormat.year, dateFormat.month, dateFormat.day % 100)
    finalIndex = INDEX + '-' + indexFormat
    print(finalIndex)

    # from and to date for ES query
    dateTime = datetime.datetime.strptime(event['time'],'%Y-%m-%d'"T" '%H:%M:%S'"Z")  # convert string to datetime object

    c_time = dateTime - timedelta(minutes=1)
    fromDateTime = c_time.strftime('%Y-%m-%d'"T"'%H:%M:00'"Z")
    print(fromDateTime)

    # Get last 1minute datetime
    ls = dateTime - timedelta(minutes=1)
    toDateTime = ls.strftime('%Y-%m-%d'"T"'%H:%M:59'"Z")
    print(toDateTime)
    query_toDateTime = ls.strftime('%Y-%m-%d'"T"'%H:%M:59.999'"Z")
    print(toDateTime)
    print(query_toDateTime)

    query = {
        "query": {
            "bool": {"filter": [{"range": {"timestamp": {"gte": fromDateTime, "lte": query_toDateTime}}}]}
        }
    }

    try:
        response = es.search(index=finalIndex, doc_type=finalIndex, body=query, size=9000)  # search from ES
        print(response)
    except Exception as e1:
        print('Query Exception ' + str(e1))

    if response['hits']['total'] != 0:
        # convert UTC to IST time for subject of alert
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('Asia/Kolkata')
        utcFrom = datetime.datetime.strptime(fromDateTime, '%Y-%m-%d'"T"'%H:%M:%S'"Z")
        utcTo = datetime.datetime.strptime(toDateTime, '%Y-%m-%d'"T"'%H:%M:%S'"Z")
        utcFrom = utcFrom.replace(tzinfo=from_zone)
        utcTo = utcTo.replace(tzinfo=from_zone)
        ist_from = utcFrom.astimezone(to_zone)
        # remove +5.30 from subject
        ist_fromDateTime = ist_from.strftime('%Y-%m-%d %H:%M:%S')  # for yy-mm-dd T H:M:S Z
        ist_to = utcTo.astimezone(to_zone)
        ist_toDateTime = ist_to.strftime('%Y-%m-%d %H:%M:%S')  # for yy-mm-dd T H:M:S.floatval Z

        # loop through all the records
        for items in response['hits']['hits']:
            # condition for getting records as per error type - RESOURCE NOT FOUND
            if items['_source']['responseType'] == RESPONSETYPE1 and items['_source']['partnerId'] == PARTNER_ID:
                # convert timestamp to IST
                resTime = datetime.datetime.strptime(items['_source']['@timestamp'], '%Y-%m-%d'"T"'%H:%M:%S.%f'"Z")
                resTime = resTime.replace(tzinfo=from_zone)
                timestampRes = resTime.astimezone(to_zone)

                # content to be send in alert
                messageRes += "timestamp: " + str(timestampRes) + " , requestId: " + items['_source']['requestId'] + " , operationName: " + items['_source']['operationName'] + "\n"
                strRes = list(filter(None, messageRes.split('\n')))  # Remove any empty string if exists
                ResCount = len(strRes)
                subjectRes = str(ResCount) + " Records with " + RESPONSETYPE1 + " from " + str(ist_fromDateTime) + " to " + str(ist_toDateTime)
            
            # condition for getting records as per error type - INTERNAL SERVER ERROR
            if items['_source']['responseType'] == RESPONSETYPE2:
                # convert timestamp to IST
                intTime = datetime.datetime.strptime(items['_source']['@timestamp'], '%Y-%m-%d'"T"'%H:%M:%S.%f'"Z")
                intTime = intTime.replace(tzinfo=from_zone)
                timestampInt = intTime.astimezone(to_zone)

                # content to be send in alert
                messageInt += "timestamp: " + str(timestampInt) + " , requestId: " + items['_source']['requestId'] + " , operationName: " + items['_source']['operationName'] + "\n"
                strInt = list(filter(None, messageInt.split('\n')))  # Remove any empty string if exists
                IntCount = len(strInt)
                subjectInt = str(IntCount) + " Records with " + RESPONSETYPE2 + " from " + str(ist_fromDateTime) + " to " + str(ist_toDateTime)
                print(IntCount)
            
            #processingTime>2200 and operationName: REPAYMENT AND operationType: CALLBACK
            if items['_source']['processingTime'] > float(PROCESSINGTIME2):
                if items['_source']['operationName'] == OPERATIONNAME3 and items['_source']['operationType'] == OPERATIONTYPE2 and items['_source']['partnerId'] == PARTNER_ID:
                    # convert timestamp to IST
                    procTime = datetime.datetime.strptime(items['_source']['@timestamp'], '%Y-%m-%d'"T"'%H:%M:%S.%f'"Z")
                    procTime = procTime.replace(tzinfo=from_zone)
                    timestampProc = procTime.astimezone(to_zone)

                    # content to be send in alert
                    messageProc += "processingTime > " + PROCESSINGTIME2 + "ms , timestamp: " + str(timestampProc) + " , requestId: " + items['_source']['requestId'] + " , operationName: " + items['_source']['operationName'] + ", operationType: " + items['_source']['operationType'] + "\n"
                    strProc = list(filter(None, messageProc.split('\n')))  # Remove any empty string if exists
                    ProCount = len(strProc)
                    subjectProc = str(ProCount) + " Records fail processingTime check from " + str(ist_fromDateTime) + " to " + str(ist_toDateTime) + " IST"
            
            #processingTime>700 and operationName:DUES_AND_BALANCES
            elif items['_source']['processingTime'] > float(PROCESSINGTIME3) and items['_source']['operationName'] == OPERATIONNAME3 and items['_source']['partnerId'] == PARTNER_ID:
                # convert timestamp to IST
                procTime = datetime.datetime.strptime(items['_source']['@timestamp'], '%Y-%m-%d'"T"'%H:%M:%S.%f'"Z")
                procTime = procTime.replace(tzinfo=from_zone)
                timestampProc = procTime.astimezone(to_zone)

                # content to be send in alert
                messageProc += "processingTime > " + PROCESSINGTIME3 + "ms , timestamp: " + str(timestampProc) + " , requestId: " + items['_source']['requestId'] + " , operationName: " + items['_source']['operationName'] + ", operationType: " + items['_source']['operationType'] + "\n"
                strProc = list(filter(None, messageProc.split('\n')))  # Remove any empty string if exists
                ProCount = len(strProc)
                subjectProc = str(ProCount) + " Records fail processingTime check from " + str(ist_fromDateTime) + " to " + str(ist_toDateTime) + " IST"
            
            # processingTime>600 for operationName: REGISTRATION, STANDING_INSTRUCTION_SETUP, CHARGE_AUTHORIZATION, CHARGE_CANCEL_AUTHORIZATION, CHARGE_CAPTURE, CHARGE_CANCEL_CAPTURE AND (operationName:REPAYMENT AND operationType:SYNC_OPERATION)
            elif items['_source']['processingTime'] > float(PROCESSINGTIME1):
                print(OPERATIONNAME_LIST)
                if items['_source']['operationName'] in OPERATIONNAME_LIST:
                    # convert timestamp to IST
                    procTime = datetime.datetime.strptime(items['_source']['@timestamp'], '%Y-%m-%d'"T"'%H:%M:%S.%f'"Z")
                    procTime = procTime.replace(tzinfo=from_zone)
                    timestampProc = procTime.astimezone(to_zone)
                    # content to be send in alert
                    messageProc += "processingTime > " + PROCESSINGTIME1 + "ms , timestamp: " + str(timestampProc) + " , requestId: " + items['_source']['requestId'] + " , operationName: " + items['_source']['operationName'] + ", operationType: " + items['_source']['operationType'] + "\n"
                    strProc = list(filter(None, messageProc.split('\n')))  # Remove any empty string if exists
                    ProCount = len(strProc)
                    subjectProc = str(ProCount) + " Records fail processingTime check from " + str(ist_fromDateTime) + " to " + str(ist_toDateTime) + " IST"
                elif items['_source']['operationName'] == OPERATIONNAME2 and items['_source']['operationType'] == OPERATIONTYPE1:
                    # convert timestamp to IST
                    procTime = datetime.datetime.strptime(items['_source']['@timestamp'], '%Y-%m-%d'"T"'%H:%M:%S.%f'"Z")
                    procTime = procTime.replace(tzinfo=from_zone)
                    timestampProc = procTime.astimezone(to_zone)

                    # content to be send in alert
                    messageProc += "processingTime > " + PROCESSINGTIME1 + "ms , timestamp: " + str(timestampProc) + " , requestId: " + items['_source']['requestId'] + " , operationName: " + items['_source']['operationName'] + ", operationType: " + items['_source']['operationType'] + "\n"
                    strProc = list(filter(None, messageProc.split('\n')))  # Remove any empty string if exists
                    ProCount = len(strProc)
                    subjectProc = str(ProCount) + " Records fail processingTime check from " + str(ist_fromDateTime) + " to " + str(ist_toDateTime) + " IST"

            # any other operationName not checked in above conditions
            else:
                # convert timestamp to IST
                if items['_source']['processingTime'] > float(PROCESSINGTIME):
                    procTime = datetime.datetime.strptime(items['_source']['@timestamp'], '%Y-%m-%d'"T"'%H:%M:%S.%f'"Z")
                    procTime = procTime.replace(tzinfo=from_zone)
                    timestampProc = procTime.astimezone(to_zone)
    
                    # content to be send in alert
                    messageProc += "processingTime > " + PROCESSINGTIME + "ms , timestamp: " + str(timestampProc) + " , requestId: " + items['_source']['requestId'] + " , operationName: " + items['_source']['operationName'] + ", operationType: " + items['_source']['operationType'] + "\n"
                    strProc = list(filter(None, messageProc.split('\n')))  # Remove any empty string if exists
                    ProCount = len(strProc)
                    subjectProc = str(ProCount) + " Records fail processingTime check from " + str(ist_fromDateTime) + " to " + str(ist_toDateTime) + " IST"
            
                
        # invoke publish to sns topic method
        if ResCount != 0 and messageRes != '':
            res = publishToSnsTopic(subjectRes, messageRes)
            subjectRes = ''
            messageRes = ''
        else:
            print("No data")

        if IntCount != 0 and messageInt != '':
            res1 = publishToSnsTopic(subjectInt, messageInt)
            subjectInt = ''
            messageInt = ''
        else:
            print("No data")

        if ProCount != 0 and messageProc != '':
            res2 = publishToSnsTopic(subjectProc, messageProc)
            subjectProc = ''
            messageProc = ''
        else:
            print("No data")
    else:
        print("No records found")


# publish to SNS topic
def publishToSnsTopic(subject, message):
    print('Subject: ' + subject)
    print('Subject Character Length: ' + str(len(subject)))
    try:
        response = client.publish(
            TopicArn=TOPICARN,
            Subject=subject,
            Message=message
        )
    except Exception as e2:
        print('SNS Publish exception ' + str(e2))
        response = None

    return response

'''def insertToTable(finalError):
    response = table.put_item(
        Item={
            'SummaryEventtime': linkedAccount,
            'INTERNAL_SERVER_ERROR_count': date_period,
            'RESOURCE_NOT_FOUND_count': yest_1_amount,
            'ERROR_600_count': lastMonth_amount,
            'error_data': finalError
        }
    )
    return response'''