import boto3
import json
import os

def lambda_handler(event, context):
    print("Boto3 Version: " + boto3.__version__)
    print("Received event: " + json.dumps(event))
    
    
    # Preparing variables using Environment Settings
    aws_region = os.getenv('AWS_REGION', 'ap-south-1')
    
    LifecycleActionToken = event["detail"].get("LifecycleActionToken")
    AutoScalingGroupName = event["detail"].get("AutoScalingGroupName")
    LifecycleHookName = event["detail"].get("LifecycleHookName")
    EC2InstanceId = event["detail"].get("EC2InstanceId")
    LifecycleTransition = event["detail"].get("LifecycleTransition")
    NotificationMetadata = event["detail"].get("NotificationMetadata")
    
    target_groups_arns = NotificationMetadata.split(",")
    TG1 = target_groups_arns[0]
    TG2 = target_groups_arns[1]
    
    # Processing
    try:
        elbv2_client = boto3.client("elbv2", region_name=aws_region)
    except Exception as ex:
        print("Client access failed: "+ex)
    
    try:
        TG1_response = elbv2_client.deregister_targets(
            TargetGroupArn=TG1,
            Targets=[
                {
                    'Id': EC2InstanceId,
                },
            ]
        )
        print("TG1_response: " + json.dumps(TG1_response))
    except Exception as ex:
        print("TG1: "+ex)
        print("Instance "+EC2InstanceId+" failed to deregister from TG1 with arn -"+TG1)
    
    try:
        TG2_response = elbv2_client.deregister_targets(
            TargetGroupArn=TG2,
            Targets=[
                {
                    'Id': EC2InstanceId,
                },
            ]
        )
        print("TG2_response: " + json.dumps(TG2_response))
    except Exception as ex:
        print("TG2: "+ex)
        print("Instance "+EC2InstanceId+" failed to deregister from TG2 with arn -"+TG2)
        
        
    try:
        ASG_client = boto3.client("autoscaling", region_name=aws_region)
    except Exception as ex:
        print("ASG Client access failed: "+ex)
        
    try:
        ASG_response = ASG_client.complete_lifecycle_action(
            LifecycleHookName=LifecycleHookName,
            AutoScalingGroupName=AutoScalingGroupName,
            LifecycleActionToken=LifecycleActionToken,
            LifecycleActionResult='ABANDON'
        )
        print("ASG_response: " + json.dumps(ASG_response))
    except Exception as ex:
        print("ASG Lifecycle Hook action failed to complete: "+ex)
        
    return
    
