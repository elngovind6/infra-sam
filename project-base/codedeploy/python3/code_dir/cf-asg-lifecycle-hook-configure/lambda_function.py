import boto3
import json
import os

def lambda_handler(event, context):
    print("Boto3 Version: " + boto3.__version__)
    print("Received event: " + json.dumps(event))


    # Preparing variables using Environment Settings
    aws_region = os.getenv('AWS_REGION', 'ap-south-1')
    
    STAGE_TG_COMMA_SEPARATED_LIST = os.getenv('STAGE_TG_COMMA_SEPARATED_LIST', 'arn:aws:elasticloadbalancing:ap-south-1:294578744445:targetgroup/stage-private-lms/5a356fcbf0aeb9b5,arn:aws:elasticloadbalancing:ap-south-1:294578744445:targetgroup/stage-public-lms/21183a2cf3b8ee32')
    PROD_TG_COMMA_SEPARATED_LIST = os.getenv('PROD_TG_COMMA_SEPARATED_LIST', 'arn:aws:elasticloadbalancing:ap-south-1:294578744445:targetgroup/prod-private-lms/06793ef4505c5d7c,arn:aws:elasticloadbalancing:ap-south-1:294578744445:targetgroup/prod-public-lms/a0950ebef2e3189c')

    LIFECYCLE_HOOK_NAME = os.getenv('LIFECYCLE_HOOK_NAME', '')
    CLOUDWATCH_EVENTS_RULE_NAME = os.getenv('CLOUDWATCH_EVENTS_RULE_NAME', '')
    CLOUDWATCH_EVENTS_RULE_TARGET_ARN = os.getenv('CLOUDWATCH_EVENTS_RULE_TARGET_ARN', '')
    CLOUDWATCH_EVENTS_RULE_ROLEARN = os.getenv('CLOUDWATCH_EVENTS_RULE_ROLEARN', '')

    # Processing
    CD_client = boto3.client("codedeploy", region_name=aws_region)
    
    applicationName = event["detail"]["application"]
    deploymentGroupName = event["detail"]["deploymentGroup"]
    deploymentId = event["detail"]["deploymentId"]    
    
    print('applicationName: '+applicationName)
    print('deploymentGroupName: '+deploymentGroupName)
    print('deploymentId: '+deploymentId)
    
    TG_COMMA_SEPARATED_LIST = STAGE_TG_COMMA_SEPARATED_LIST
    if deploymentGroupName == 'prod-lms-app-dg':
        TG_COMMA_SEPARATED_LIST = PROD_TG_COMMA_SEPARATED_LIST
    
    deployment_group_info = CD_client.get_deployment_group(
            applicationName = applicationName,
            deploymentGroupName = deploymentGroupName
        )
    print(deployment_group_info["deploymentGroupInfo"])        
    dg_asg_name = deployment_group_info["deploymentGroupInfo"]["autoScalingGroups"][0].get("name")
    
    print('DG asg_name: '+dg_asg_name)
    
    asg_name_by_deploymentId = 'CodeDeploy_'+deploymentGroupName+'_'+deploymentId
    
    asg_name = asg_name_by_deploymentId
    
    print('deploymentId asg_name: '+asg_name)

    ASG_client = boto3.client("autoscaling", region_name=aws_region)
    
    try:
        asg = ASG_client.describe_auto_scaling_groups(
            AutoScalingGroupNames=[
                asg_name
            ]
        )
        if len(asg["AutoScalingGroups"]) > 0:
            asg_name = asg_name
        else:
            try:
                asg = ASG_client.describe_auto_scaling_groups(
                    AutoScalingGroupNames=[
                        dg_asg_name
                    ]
                )
                asg_name = dg_asg_name
            except Exception as asg_ex2:
                print("Call 2. Problem during asg details extraction: "+str(asg_ex2))
    except Exception as asg_ex1:
        print("Call 1. Problem during asg details extraction: "+str(asg_ex1))

    print('Final asg_name: '+asg_name)

    # 1
    try:
      lifecycle_configuration_response = ASG_client.put_lifecycle_hook(
          LifecycleHookName=LIFECYCLE_HOOK_NAME,
          AutoScalingGroupName=asg_name,
          LifecycleTransition='autoscaling:EC2_INSTANCE_TERMINATING',
          NotificationMetadata=TG_COMMA_SEPARATED_LIST,
          HeartbeatTimeout=600,
          DefaultResult='ABANDON'
      )
      print("lifecycle_configuration_response: "+json.dumps(lifecycle_configuration_response))
    except Exception as ex:
      print("lifecycle_configuration_response Exception: "+str(ex))          
    
    """
    CW_Events_client = boto3.client("events", region_name=aws_region)
    
    try:
      EventPattern = '''
        {
          "source": [
            "aws.autoscaling"
          ],
          "detail-type": [
            "EC2 Instance-terminate Lifecycle Action"
          ],
          "detail": {
            "AutoScalingGroupName": [
              "'''+asg_name+'''"
            ]
          }
        }'''
      print("EventPattern: "+EventPattern)
    except Exception as ex:
      print("EventPattern Exception: "+str(ex))
    
    # 2
    try:
      CW_Events_putRule_response = CW_Events_client.put_rule(
          Name=CLOUDWATCH_EVENTS_RULE_NAME,
          EventPattern=EventPattern,
          State='ENABLED',
          Description='asg lifecycle hook action event rule',
          RoleArn=CLOUDWATCH_EVENTS_RULE_ROLEARN
      )
      print("CW_Events_putRule_response: "+json.dumps(CW_Events_putRule_response))
    except Exception as ex:
      print("CW_Events_putRule_response Exception: "+str(ex))
    

    # 3
    try:
      CW_Events_RemoveTargets_response = CW_Events_client.remove_targets(
        Rule=CLOUDWATCH_EVENTS_RULE_NAME,
        Ids=[
            '1',
        ]
      )
      print("CW_Events_RemoveTargets_response: "+json.dumps(CW_Events_RemoveTargets_response))
    except Exception as ex:
      print("CW_Events_RemoveTargets_response Exception: "+str(ex))
    
    # 4
    try:
      CW_Events_PutTargets_response = CW_Events_client.put_targets(
          Rule=CLOUDWATCH_EVENTS_RULE_NAME,
          Targets=[
              {
                  'Id': '1',
                  'Arn': CLOUDWATCH_EVENTS_RULE_TARGET_ARN,
              },
          ]
      )
      print("CW_Events_PutTargets_response: "+json.dumps(CW_Events_PutTargets_response))
    except Exception as ex:
      print("CW_Events_PutTargets_response Exception: "+str(ex))
    """
    
    print("Lambda Function Completed")

