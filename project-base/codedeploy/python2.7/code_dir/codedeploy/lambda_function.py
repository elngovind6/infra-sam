import boto3
import json
# import re

def lambda_handler(event, context):
    print("Received event: " + json.dumps(event))
    CD_client = boto3.client("codedeploy")
    deployment_group_info = CD_client.get_deployment_group(
            applicationName =event["detail"]["application"],
            deploymentGroupName = event["detail"]["deploymentGroup"] 
        )
    CD_request=event["detail"]["state"]
    if CD_request=="FAILURE" or CD_request=="STOP":
        if deployment_group_info["deploymentGroupInfo"]["autoScalingGroups"]==[]:
            print("Stand alone instance, no action required")
        else:
            deploymentGroup=event["detail"]["deploymentGroup"]
            deploymentId=event["detail"]["deploymentId"]
            
            # Using CloudTrail log is the most secure way to retrieve ASG name however it might tooks up to 15 mins
            # to be avaliable.In that case you will need to add cloudtrail read permission to the Lambda role.  
            # Using the fixed CodeDeploy B/G ASG name pattern for now
        
            #ct_client=boto3.client('cloudtrail',region_name='ap-southeast-2')
            #response = ct_client.lookup_events(
            #    LookupAttributes=[
            #        {
            #            'AttributeKey': 'EventName',
            #            'AttributeValue': 'CreateAutoScalingGroup'
            #       },
            #    ],
            #    MaxResults=10,
            #)
            
            #for item in response['Events']:
            #    if item['Username']==re.sub('-','',event["detail"]["deploymentId"]):
            #        resource=json.loads(item['CloudTrailEvent'])
            
            #asg_name = resource['requestParameters']['autoScalingGroupName']
        
                
            asg_name = '{}{}{}{}'.format('CodeDeploy_',deploymentGroup,'_',deploymentId)
            print (asg_name)
            try:
                asg_client = boto3.client('autoscaling')
                response = asg_client.delete_auto_scaling_group(
                    AutoScalingGroupName=asg_name,
                    ForceDelete=True
                )
                print('{}{}'.format(asg_name,' deleted'))
            except:
                raise


