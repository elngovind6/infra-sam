import boto3
import json
import logging
import os
import users_detail

from base64 import b64decode
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

ApiList = ["SAFE", "LMS", "TOR", "PAYLATER", "CFAPPS", "BANKING", "EMANDATE", "PAYTM", "RC", "SANCTION", "UCIC", "CHECKOUT", "CIBIL", "WORKFLOW", "AADHAAR", "FLASK", "APPS", "CFDB", "MYSQL", "THEBOX", "MINOTY", "DBCONTROL", "DEPLOY_R", "GATEWAY", "JENKINS", "EXPERIAN", "REVERSE_PROXY", "ELK", "VPN", "BACKUPS", "MONGO", "IDFC", "NPS", "ALERTS", "DW_TASKS", "DS_PYTHON", "PRIVATE-RESPONSE", "PUBLIC-RESPONSE",  "POSTGRES-MIGRATION", "ACTIVITY-DEBUG", "AUTHENTICATION", "PRIVATE-ELB", "PUBLIC-ELB", "DOCGEN"]
CRITICAL = "CRITICAL"
logger = logging.getLogger()
logger.setLevel(logging.INFO)



def lambda_handler(event, context):
    logger.info("Event: " + str(event))
    message = json.loads(event['Records'][0]['Sns']['Message'])
    logger.info("Message: " + str(message))
    
    users = users_detail.dict

    alarm_name = message['AlarmName']
    for item in ApiList:
        if item in alarm_name and CRITICAL in alarm_name:
           new_state = message['NewStateValue']
           reason = message['NewStateReason']
           for api , api_owner in users.items():
               if api == item:
                  for user  in api_owner:
                      Channel = user.get("Channel")
                      Webhook = user.get("Webhook")
                      slack_message = {
                      'channel': Channel,
                      'text': "%s state is now %s: %s" % (alarm_name, new_state, reason)
                      }
                      req = Request(Webhook, json.dumps(slack_message).encode('utf-8'))
                      try:
                          response = urlopen(req)
                          response.read()
                          logger.info("Message posted to %s", slack_message['channel'])
                      except HTTPError as e:
                          logger.error("Request failed: %d %s", e.code, e.reason)
                      except URLError as e:
                          logger.error("Server connection failed: %s", e.reason)
