Shubham = {'Channel': '@shubham.aggarwal', 'Webhook': 'https://hooks.slack.com/services/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'}
Abhijith = {'Channel': '@abhijith', 'Webhook': 'https://hooks.slack.com/services/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'}
Abhinav = {'Channel': '@abhinav', 'Webhook': 'https://hooks.slack.com/services/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'}
Mohit = {'Channel': '@mohit.kaushal', 'Webhook': 'https://hooks.slack.com/services/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'}
Ankur = {'Channel': '@ankur.panchani', 'Webhook': 'https://hooks.slack.com/services/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'}
Mohan = {'Channel': '@mohan.kumar', 'Webhook': 'https://hooks.slack.com/services/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'}
Anirudh = {'Channel': '@anirudh.parui', 'Webhook': 'https://hooks.slack.com/services/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'}
Komal = {'Channel': '@komal.kumar', 'Webhook': 'https://hooks.slack.com/services/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'}
Sriharsha ={'Channel': '@sriharsha.vattikuti', 'Webhook': 'https://hooks.slack.com/services/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'}
Monisha ={'Channel': '@monisha.singh', 'Webhook': 'https://hooks.slack.com/services/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'}
Bhargava ={'Channel': '@bhargava.k', 'Webhook': 'https://hooks.slack.com/services/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'}
Rohit = {'Channel': '@rohit.soni', 'Webhook': 'https://hooks.slack.com/services/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'}
Radhika = {'Channel': '@radhika.muppala', 'Webhook': 'https://hooks.slack.com/services/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'}
Govind = {'Channel': '@govind.kumar', 'Webhook': 'https://hooks.slack.com/services/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'}
Ankit = {'Channel': '@ankit.thakur', 'Webhook': 'https://hooks.slack.com/services/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'}
Sanket = {'Channel': '@sanket.gund', 'Webhook': 'https://hooks.slack.com/services/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'}
Aditya = {'Channel': '@adityasharma', 'Webhook': 'https://hooks.slack.com/services/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'}
Asma = {'Channel': '@asma.khan', 'Webhook': 'https://hooks.slack.com/services/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'}
Ashish = {'Channel': '@ashish.ranjan', 'Webhook': 'https://hooks.slack.com/services/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'}
Gnoc = {'Channel': '@cf-gnoc', 'Webhook': 'https://hooks.slack.com/services/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'}
Abhidarshanam = {'Channel': '@abhidarshanam', 'Webhook': 'https://hooks.slack.com/services/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'}
Kajal = {'Channel': '@kajal.singh', 'Webhook': 'https://hooks.slack.com/services/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'}
Sarath = {'Channel': '@sarath.kumar', 'Webhook': 'https://hooks.slack.com/services/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'}

dict = {
  'TOR': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'CFAPPS': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'BANKING': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'EMANDATE': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'PAYLATER': [Abhijith,Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'PAYTM': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'RC': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'SANCTION': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'UCIC': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'CFDB': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'APP': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'MYSQL': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'CIBIL': [Abhijith, Shubham, Mohit, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'WORKFLOW': [Abhijith, Shubham, Mohan, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'CHECKOUT': [Abhinav, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'LMS': [Mohit, Ankur, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'SAFE': [Ankur, Anirudh, Abhijith, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'FLASK': [Komal, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Abhijith, Abhinav, Sarath],
  'AADHAAR': [Abhinav, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'MINOTY': [Anirudh, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'THEBOX': [Komal, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'DBCONTROL': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'DEPLOY_R': [Abhijith, Sriharsha, Monisha, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'GATEWAY': [Abhijith, Sriharsha, Monisha, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'JENKINS': [Abhijith, Sriharsha, Monisha, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'REVERSE_PROXY': [Abhijith, Sriharsha, Monisha, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'BACKUPS': [Abhijith, Sriharsha, Monisha, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'VPN': [Abhijith, Sriharsha, Monisha, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'EXPERIAN': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'ELK': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'IDFC': [Abhijith, Bhargava, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'NPS': [Abhijith, Komal, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'ALERTS': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'DW_TASKS': [Abhijith, Bhargava, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'DS_PYTHON': [Abhijith, Abhinav, Rohit, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'MONGO': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'PUBLIC-RESPONSE': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'PRIVATE-RESPONSE': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'ACTIVITY-DEBUG': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'POSTGRES-MIGRATION': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'AUTHENTICATION': [Abhijith, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'PRIVATE-ELB': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'PUBLIC-ELB': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath],
  'DOCGEN': [Abhijith, Shubham, Radhika, Ankit, Govind, Aditya, Sanket, Ashish, Gnoc, Abhidarshanam, Kajal, Sarath]
}