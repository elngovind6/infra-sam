import boto3
import sys
import time
import json
def lambda_handler(event, context):
    # TODO implement
    def tag():
        client = boto3.client('ec2', region_name='ap-south-1')
        response = client.describe_instances(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [
                        'vpc-0284483b4e0457ad2','vpc-049bf1cfb9eb7ff9d','vpc-1dc08874','vpc-357fa85c','vpc-68359100','vpc-7de0e714','vpc-b5cb7cdc','vpc-b95abcd0',
                    ]
                },
            ]
        )
        id = []
        out = response.get('Reservations', None)
        if out:
            for data in out:
                resp = data.get('Instances')
                for item in resp:
                    for value in item.get('Tags'):
                        if(value.get("Key") != 'Owner') or (value.get("Key") != 'owner'):
                           id.append(item.get('InstanceId'));
        return id
    instances_list=list(set(tag()))
    print (instances_list)
    outp=[]
    i= []
    j={}
    for value in instances_list:
        def cloud_trail():
            client = boto3.client('cloudtrail', region_name='ap-south-1')
            response = client.lookup_events(
            LookupAttributes=[
                {
                    'AttributeKey': 'ResourceName',
                    'AttributeValue': value
                },
            ]
            )
            out = response.get('Events', None)
            if out:
                for eventid in out:
                    if eventid.get("EventName") == 'RunInstances':
                        # outp.append(str(eventid.get('Username')));
                        # print(str(eventid.get('Username')).split('@')[0]+("@capitalfloat.com"))
                        j = {
                            value: str(eventid.get('Username')).split('@')[0]+("@capitalfloat.com"),
                            }
                        i.append(j)
                        #print(value+" RunInstances "+eventid.get("Username"));
                    #else:
                        #print(value+"-"+eventid.get("EventName")+eventid.get("Username"));
            return i
        cloud_trail()
    print (i)
    def sns():
        client = boto3.client('sns', region_name='ap-south-1')
        response = client.publish(
            TopicArn='arn:aws:sns:ap-south-1:294578744445:TagsMonitoring',
            Message='Instances that doesnt have tags:'+ str(instances_list)+ 'List of users who created these instances been gathered from cloud trail' + str(i) ,
            Subject='Tags Monitoring',
            )
        return response
    # print(sns())
    return {
        'statusCode': 200,
        'body': json.dumps('Please check output!')
    }
