import boto3
import sys
import time
import json

client = boto3.client('ec2')
regions = [region['RegionName'] for region in client.describe_regions()['Regions']]
# region = "ap-south-1"


def lambda_handler(event, context):
# def lamb():
    i= []
    details = []
    all_instances = []
    for region in regions:
        vpc_list=[]
        def get_vpcs(): # not using this function curerntly
            client = boto3.client('ec2', region_name=region)
            response = client.describe_vpcs()
            output = response.get('Vpcs', None)
            if output:
                for value in output:
                    vpc_id = value.get('VpcId')
                    vpc_list.append(vpc_id)
            return vpc_list
        vpc_list = get_vpcs()
        # print("List of VPC's that we are looking")
        # print(vpc_list)
        client = boto3.client('ec2', region_name=region)
        response = client.describe_instances(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': vpc_list
                },
            ]
        )
        id = []
        out = response.get('Reservations', None)
        if out:
            for data in out:
                resp = data.get('Instances')
                for item in resp:
                    flag = False
                    Key = "Tags"
                    if Key in item.keys():
                        for value in item.get(Key):
                            if value.get("Key").lower() == 'owner':
                                flag = True
                                continue
                        if not flag:
                            id.append(item.get('InstanceId'))
                            all_instances.append(item.get('InstanceId'))
                    else:
                        id.append(item.get('InstanceId'))
                        all_instances.append(item.get('InstanceId'))
        instances_list=list(set(id))
        # print ("number of machines which doesnt have owner tag:  " + str(len (instances_list)))
        # print (instances_list)
        j={}
        if instances_list:
            d= {region : instances_list}
            details.append(d)
            for instanceid in instances_list:
                client = boto3.client('cloudtrail', region_name=region)
                response = client.lookup_events(
                LookupAttributes=[
                    {
                        'AttributeKey': 'ResourceName',
                        'AttributeValue': instanceid
                    },
                ],
                MaxResults=1000,
                )
                out = response.get('Events', None)
                if out:
                    for eventid in out:
                        if eventid.get("EventName") == 'RunInstances':
                            j = {
                                instanceid: str(eventid.get('Username')).split('@')[0]+("@capitalfloat.com"),
                                }
                            i.append(j)
    if all_instances:
        print (i)
        print ("total:  "+str(len(all_instances)))
        def sns():
            client = boto3.client('sns', region_name='ap-south-1')
            response = client.publish(
                TopicArn='arn:aws:sns:ap-south-1:294578744445:TagsMonitoring',
                Message='Hi, \n \n Number of Infra account machines which doesnt have owner tag in all regions:'  +str(len(all_instances))+ '\n \n Instances list in Infra account that doesnt have tags in all regions:  \n '+ str(details)+'  \n \n \n List of users who created these instances been gathered from cloud trail with instance id:   \n ' + str(i) ,
                Subject='Tags Monitoring',
                )
            return response
        print(sns())
        return {
            'statusCode': 200,
            'body': json.dumps('Please check output!')
        }
    else:
        print("no instances to show")
        return {
            'statusCode': 200,
            'body': json.dumps('Please check output!')
        }
